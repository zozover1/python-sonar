import unittest

def calculate_area_rectangle(length, long):
    return length * long

area = calculate_area_rectangle("A", "A")
print("L'aire du rectangle est :", area)

class TestCalculateAreaRectangle(unittest.TestCase):

    def test_area_with_positive_values(self):
        self.assertEqual(calculate_area_rectangle(2, 4), 8, "Should be 8")

    def test_area_with_zero_values(self):
        self.assertEqual(calculate_area_rectangle(0, 0), 0, "Should be 0")

    def test_area_with_negative_values(self):
        self.assertEqual(calculate_area_rectangle(-2, -4), 8, "Should be 8")
        
    def test_area_with_floats(self):
        self.assertAlmostEqual(calculate_area_rectangle(2.5, 4.5), 11.25, "Should be 11.25")      
        
    def test_area_with_alphabetic_values(self):
        self.assertEqual(calculate_area_rectangle("A", 4), "AAAA", "Should be AAAA")
        
    def test_area_with_negative_alphabetic_values(self):
        self.assertEqual(calculate_area_rectangle("A", -4), "", "Should be nothing")
        
    def test_area_with_only_alphabetic_values(self):
        self.assertEqual(calculate_area_rectangle("A", "A"), "TypeError: can't multiply sequence by non-int of type 'str'", "Should be 'Erreur: Les valeurs doivent être numériques.'")

if __name__ == '__main__':
    unittest.main()