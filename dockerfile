# Utilisation de l'image Python 3.8 slim comme base
FROM python:3.8-slim

# Mise à jour des sources et installation de wget, gnupg et unzip
RUN apt-get update && apt-get install -y wget gnupg unzip

# Téléchargement du fichier zip contenant le pilote Chrome ARM64
RUN wget -O /tmp/chrome-mac-arm64.zip https://storage.googleapis.com/chrome-for-testing-public/123.0.6312.86/mac-arm64/chromedriver-mac-arm64.zip

# Extraction du pilote Chrome
RUN unzip /tmp/chrome-mac-arm64.zip -d /usr/local/bin

# Assurez-vous que le pilote Chrome a les autorisations d'exécution appropriées
RUN chmod +x /usr/local/bin

# Nettoyage du cache et des paquets inutiles
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Copiez les fichiers Python
COPY test-epsi.py ./app/test-epsi.py

# Installation des dépendances Python
RUN pip install selenium

# Définir le répertoire de travail
WORKDIR /app

# Commande par défaut à exécuter lorsque le conteneur est démarré
CMD ["python", "test-epsi.py"]


